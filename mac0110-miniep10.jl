
function submatriz(matriz,tamanho, pos_col, pos_lin)
    subs = zeros(tamanho, tamanho)
    for i in 1:tamanho
        for j in 1:tamanho
            subs[i,j] = matriz[i + pos_col, j + pos_lin]
        end
    
    end
    return subs
end

function tds_submatrizes(matriz)
    tam_mtx = size(matriz)[1]
    tds = Dict()
    ponteiro_linha = 0
    ponteiro_coluna = 0
    tamanho = 1
    aux = 1
    while tamanho <= tam_mtx
        for i in 0:tam_mtx - tamanho
            for j in 0:tam_mtx - tamanho
                tds[aux] = submatriz(matriz,tamanho, i, j)
                aux += 1
            end
        end
        
        tamanho += 1
    end
    return tds
end

function soma_matriz(matriz)
    soma = 0
    for i in 1:length(matriz)
        soma += matriz[i]
    end
    return soma
    
end

function max_sum_submatrix(matriz)
    dic_submatrizes = tds_submatrizes(matriz)
    somas = []
    
    for i in 1:length(dic_submatrizes)
        append!(somas,soma_matriz(dic_submatrizes[i])) 
        
    end
    res = []
    for j in 1:length(dic_submatrizes)
        if somas[j] == findmax(somas)[1]
            append!(res,[dic_submatrizes[j]])
        end
    end
    return res


end

matriz1 = [1 1 1 ; 1 1 1 ; 1 1 1]
matriz2 = [-1 1 1 ; 1 -1 1 ; 1 1 -2]

using Test
function test()
    @test max_sum_submatrix(matriz1) == [1.0 1.0 1.0; 1.0 1.0 1.0; 1.0 1.0 1.0]
    @test max_sum_submatrix(matriz2) == [[1.0 1.0; -1.0 1.0],[1.0 -1.0; 1.0 1.0],[-1.0 1.0 1.0; 1.0 -1.0 1.0; 1.0 1.0 -2.0]]


end

test()

